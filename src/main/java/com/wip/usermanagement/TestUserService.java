/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.usermanagement;

/**
 *
 * @author WIP
 */
public class TestUserService {

    public static void main(String[] args) {
        UserService.addUser("nook", "password");
        System.out.println(UserService.getUser());
        UserService.addUser("nookzaa", "password");
        System.out.println(UserService.getUser());

        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("123456");
        UserService.updateUser(3, user);
        System.out.println(UserService.getUser());
        
        UserService.delUser(user);
        System.out.println(UserService.getUser());
        
        System.out.println(UserService.login("admin", "password"));

    }
}
