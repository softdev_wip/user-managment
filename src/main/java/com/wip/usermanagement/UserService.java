/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author WIP
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    //Mockup
    static {
        userList.add(new User("admin", "123456"));
        userList.add(new User("WIP", "123456"));
    }

    //Create
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }

    //Update
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    //Read 1 user
    public static User getUser(int index) {
        if (index > userList.size() - 1) {
            return null;
        }
        return userList.get(index);
    }

    //Read all user
    public static ArrayList<User> getUser() {
        return userList;
    }

    //Search username
    public static ArrayList<User> getUser(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return list;
    }

    //Delete index
    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    //Delete user
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    //Login
    public static User login(String userName, String password) {
        for (User user : userList) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }
    
    public static void save() {
        
    }
    
    public static void load() {
        
    }

}
